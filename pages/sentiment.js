import Head from "next/head"
import Link from 'next/link'

import Layout from '../components/Layout'
import BannerSentiment from '../components/BannerSentiment'
import {Pie} from 'react-chartjs-2';


export default () => {

    var options = {
        legend: {
          position: "bottom",
          labels: {
            usePointStyle: true,
            fontSize: 10,
            padding	: 15,
        }
        },
        segmentShowStroke : false
    };

    const data = {
        labels: [
            'Terrible',
            'Bad',
            'Neutral',
            'Good',
            'Great'
        ],
        datasets: [{
            data: [1, 11, 61, 19, 8],
            backgroundColor: [
                "#A6A6A8",
                "#B1E5F2",
                "#272635",
                "#CECECE",
                "#E8E9F3",
                "#B0DE09"
            ],
            hoverBackgroundColor: [
                "#A6A6A8",
                "#B1E5F2",
                "#272635",
                "#CECECE",
                "#E8E9F3",
                "#B0DE09"
            ],
            borderWidth: 0
        }],
    };

    const data2 = {
        labels: [
            'Terrible',
            'Bad',
            'Neutral',
            'Good',
            'Great'
        ],
        datasets: [{
            data: [28.1, 27, 19.1, 7.9, 18],
            backgroundColor: [
                "#A6A6A8",
                "#B1E5F2",
                "#272635",
                "#CECECE",
                "#E8E9F3",
                "#B0DE09"
            ],
            hoverBackgroundColor: [
                "#A6A6A8",
                "#B1E5F2",
                "#272635",
                "#CECECE",
                "#E8E9F3",
                "#B0DE09"
            ],
            borderWidth: 0
        }]
    };

    const data3 = {
        labels: [
            'Terrible',
            'Bad',
            'Neutral',
            'Good',
            'Great'
        ],
        datasets: [{
            data: [9.1, 10.2, 59.1, 15.9, 5.7],
            backgroundColor: [
                "#A6A6A8",
                "#B1E5F2",
                "#272635",
                "#CECECE",
                "#E8E9F3",
                "#B0DE09"
            ],
            hoverBackgroundColor: [
                "#A6A6A8",
                "#B1E5F2",
                "#272635",
                "#CECECE",
                "#E8E9F3",
                "#B0DE09"
            ],
            borderWidth: 0
        }]
    };

    const data4 = {
        labels: [
            'Negative',
            'Neutral',
            'Positive'
        ],
        datasets: [{
            data: [152, 403, 625],
            backgroundColor: [
                "#A6A6A8",
                "#B1E5F2",
                "#272635",
                "#CECECE",
                "#E8E9F3",
                "#B0DE09"
            ],
            hoverBackgroundColor: [
                "#A6A6A8",
                "#B1E5F2",
                "#272635",
                "#CECECE",
                "#E8E9F3",
                "#B0DE09"
            ],
            borderWidth: 0
        }]
    };

    return (
        <Layout>
            <Head>
                <title>Sentiment Analysis</title>
                <meta name="description" content="Sentiment Analysis" />
            </Head>

            <div>
                <BannerSentiment />

                <div id="main">
                    <section id="one">
                        <div className="inner">
                            <header className="major">
                                <h2>Sentiment Analysis</h2>
                            </header>
                            <p>Sentiment analysis is a way to treat “opinions, sentiments and subjectivity of text” through computational processes (Medha, Hassan &amp; Korashy 2014:1). Thus, it is a way to discover people's opinions towards subject-matter online.</p>
                        </div>
                    </section>
                    <section id="two" className="spotlights">
                        <section>
                            <div className="chart">
                                <Pie
                                    data={data4}
                                    width={200}
                                    height={200}
                                    options={options}
                                />
                            </div>
                            <div className="content">
                                <div className="inner">
                                    <header className="major">
                                        <h3>Overall sentiment of collected Tweets</h3>
                                    </header>
                                    <p>Because Belonging Bot is aiming to create a positive space on Twitter, the majority of the tweets that it has interacted with and collected are positive. Neutral tweets have been collected, as it is often news sources sharing these, and negative tweets have been interacted with, as Belonging Bot aims to further understand social connectedness and, in doing so, has found that the users posing negative tweets are often aiming to find a sense of social connectedness online.</p>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div className="chart">
                                <Pie
                                    data={data}
                                    width={200}
                                    height={200}
                                    options={options}
                                />
                            </div>
                            <div className="content">
                                <div className="inner">
                                    <header className="major">
                                        <h3>#lockdownSouthAfrica</h3>
                                    </header>
                                    <p>Tweets that contained the hashtag #lockdownSouthAfrica were predominantly neutral. This could be due to the fact that many of these tweets were shared by news outlets in order to inform citizens of what level South Africa was on at the time, as well as the restrictions that came with that level. A lot of the information that was shared within this folksonomy was factual and informative rather than emotional.</p>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div className="chart">
                                <Pie
                                    data={data2}
                                    width={200}
                                    height={200}
                                    options={options}
                                />
                            </div>
                            <div className="content">
                                <div className="inner">
                                    <header className="major">
                                        <h3>#Loneliness</h3>
                                    </header>
                                    <p>More than half of the tweets that were shared within this folksonomy had negative sentiments. This could be due to the fact that loneliness is usually associated with feeling sad or unsupported by the people around you. Users who share tweets with a negative sentiment within this folksonomy could be aiming to develop a sense of social connectedness online in order to alleviate those feelings.</p>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div className="chart">
                                <Pie
                                    data={data3}
                                    width={200}
                                    height={200}
                                    options={options}
                                />
                            </div>
                            <div className="content">
                                <div className="inner">
                                    <header className="major">
                                        <h3>#Belonging</h3>
                                    </header>
                                    <p>Tweets that contained the hashtag #belonging contained the most varied sentiments of the folksonomies that I explored. The majority of them were neutral, which suggests that they were either informative, or also aiming to gain insight on how people are coping with the societal lockdown. Positive sentiments were the second largest group within this folksonomy, which could allude to the fact that people are creating and experiencing a sense of belonging on Twitter. The negative sentiments expressed were mostly from users who were not experiencing a sense of belonging, and expressing their disappointment.</p>
                                </div>
                            </div>
                        </section>
                    </section>
                </div>

            </div>
        </Layout>
    )
}
