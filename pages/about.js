import Link from 'next/link'

import Layout from '../components/Layout'
import Banner from '../components/Banner'
import About from '../components/About'

export default () => (
    <Layout>
        <div>
            <Banner />

            <About />

            <p className="inner">For clarification on the terminology being used, please go to <Link href="/definitions">Definitions</Link></p>

        </div>
    </Layout>
)
