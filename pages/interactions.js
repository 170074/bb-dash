import Head from "next/head"
import Link from 'next/link'

import Layout from '../components/Layout'
import BannerInteractions from '../components/BannerInteractions'
import {Bar, Pie} from 'react-chartjs-2';


export default () => {

    var options = {
        legend: {
          position: "bottom",
          labels: {
            usePointStyle: true,
            fontSize: 10,
            padding	: 15,
        }
        },
        segmentShowStroke : false
    };

    const data = {
        labels: ['2020-09-11', '2020-09-12', '2020-09-13', '2020-09-14', '2020-09-15', '2020-09-16', '2020-09-17', '2020-09-18', '2020-09-19', '2020-09-20', '2020-09-21', '2020-09-22', '2020-09-23', '2020-09-24', '2020-09-25', '2020-09-26', '2020-09-27', '2020-09-28', '2020-09-29', '2020-09-30', '2020-10-01', '2020-10-02', '2020-10-03', '2020-10-04', '2020-10-05', '2020-10-06', '2020-10-07', '2020-10-08' ],
        datasets: [
          {
            label: 'Impressions',
            backgroundColor: '#B1E5F2',
            borderColor: '#B1E5F2',
            borderWidth: 1,
            hoverBackgroundColor: '#B1E5F2',
            hoverBorderColor: '#B1E5F2',
            data: [0, 0, 0, 0, 0, 0, 70, 1, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 4, 0, 1, 17, 2, 0, 54, 118, 25]
          }
        ]
    };

    const data2 = {
        labels: ['2020-09-11', '2020-09-12', '2020-09-13', '2020-09-14', '2020-09-15', '2020-09-16', '2020-09-17', '2020-09-18', '2020-09-19', '2020-09-20', '2020-09-21', '2020-09-22', '2020-09-23', '2020-09-24', '2020-09-25', '2020-09-26', '2020-09-27', '2020-09-28', '2020-09-29', '2020-09-30', '2020-10-01', '2020-10-02', '2020-10-03', '2020-10-04', '2020-10-05', '2020-10-06', '2020-10-07', '2020-10-08' ],
        datasets: [
          {
            label: 'Engagements',
            backgroundColor: '#CECECE',
            borderColor: '#CECECE',
            borderWidth: 1,
            hoverBackgroundColor: '#CECECE',
            hoverBorderColor: '#CECECE',
            data: [0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24, 2]
          }
        ]
    };


    return (
        <Layout>
            <Head>
                <title>Interactions</title>
                <meta name="description" content="Sentiment Analysis" />
            </Head>

            <div>
                <BannerInteractions />

                <div id="main">
                    <section id="one">
                        <div className="inner">
                            <header className="major">
                                <h2>Interactions</h2>
                            </header>
                            <p>Belonging Bot had interactions in the form oof impressions and engagements. Impressions happen when someone sees Belonging Bot's tweets or profile, whereas engagements happen when users tweet at or respond to Belonging Bot, or like or retweet its tweeets.</p>
                        </div>
                    </section>
                    <section id="two" className="spotlights">
                        <section>
                            <div className="content">
                                <Bar
                                    data={data}
                                    width={400}
                                    height={200}
                                    options={options}
                                />
                            </div>
                            <div className="content">
                                <div className="inner">
                                    <header className="major">
                                        <h3>Belonging Bot's Impressions</h3>
                                    </header>
                                    <p>The act of exploring another user’s page, and finding similar interests and experiences could be a factor in gaining a feeling of social connectedness. Maslow’s Third Need states that people will aim to form part of a group. One of the first steps to being included in a group is to share a similar interest. By having users click on Belonging Bot’s page, it can be assumed that they found a post interesting and relevant to themselves and chose to explore more. Throughout this practical project, I have found that Belonging Bot received more impressions when its own, original content was posted, as opposed to when it reposted others’ tweets.</p>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div className="content">
                                <div className="inner">
                                    <header className="major">
                                        <h3>Blonging Bot's Engagements</h3>
                                    </header>
                                    <p>Belonging Bot gained more followers when it began posting statuses of its own, as opposed to just sharing other users’ posts. This is evident with an increase of 829% profile visits in the 28 days since Belonging Bot began posting statuses of its own. These statuses consisted of extracts from my research question, quotes from sources that I researched, and emoticons such as hearts to attempt to brighten up Belonging Bot’s profile and allow it to seem welcoming. Another insight of social connectedness on Twitter that I was able to draw from Belonging Bot’s interactions is that reaching out to other users will often result in the user reciprocating the effort.</p>
                                </div>
                            </div>
                            <div className="content">
                                <Bar
                                    data={data2}
                                    width={400}
                                    height={200}
                                    options={options}
                                />
                            </div>
                        </section>
                        
                    </section>
                </div>

            </div>
        </Layout>
    )
}
