import Head from "next/head"
import {Pie} from 'react-chartjs-2';

import Layout from '../components/Layout'
import BannerTypes from '../components/BannerType'

export default () => {
    var options = {
        legend: {
          position: "bottom",
          labels: {
            usePointStyle: true,
            fontSize: 10,
            padding	: 15,
        }
        },
        segmentShowStroke : false
    };

    const data = {
        labels: [
            'Mobile',
            'Desktop'
        ],
        datasets: [{
            data: [73.38, 25.42],
            backgroundColor: [
                "#B1E5F2",
                "#272635"
            ],
            hoverBackgroundColor: [
                "#B1E5F2",
                "#272635"
            ],
            borderWidth: 0
        }],
    };

    const data2 = {
        labels: [
            'Own',
            'Replies',
            'Shares'
        ],
        datasets: [{
            data: [55.70, 24.84, 19.46],
            backgroundColor: [
                "#272635",
                "#A6A6A8",
                "#B1E5F2"
            ],
            hoverBackgroundColor: [
                "#272635",
                "#A6A6A8",
                "#B1E5F2"
            ],
            borderWidth: 0
        }],
    };

    return (
        <Layout>
            <Head>
                <title>Interaction Types</title>
                <meta name="description" content="Folksonomy Demographics" />
            </Head>

            <div>
                <BannerTypes />

                <div id="main">
                    <section id="two" className="spotlights">
                        <section>
                            <div className="chart" style={{margin: 'auto 0'}}>
                                <Pie
                                    data={data}
                                    width={200}
                                    height={200}
                                    options={options}
                                />
                            </div>
                            <div className="content">
                                <div className="inner">
                                    <header className="major">
                                        <h3>Device</h3>
                                    </header>
                                    <br/><br/>
                                    <p>According to Hootsuite “a social media management platform” (Simpson 2020), South Africa’s social media usage increased by 50% in the first two months after the lockdown was implemented, with Twitter’s usage increasing by 14.09%. When looking at device ownership of internet users aged between 16 and 64, 94% own mobile phones, compared to 76% who own laptops or desktop computers. The web traffic of mobile phones is 73.4% compared to only 24.5% for laptops and desktops. this shows that more users in South Africa are connecting to the internet, and by extension social media, on their mobile devices. Hootsuite reported that 96% of the users aged between 16 and 64 use social networking mobile applications. Twitter falls within this realm.  </p>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div className="chart" style={{margin: 'auto 0'}}>
                                <Pie
                                    data={data2}
                                    width={200}
                                    height={200}
                                    options={options}
                                />
                            </div>
                            <div className="content">
                                <div className="inner">
                                    <header className="major">
                                        <h3>Content Type</h3>
                                    </header>
                                    <br/><br/>
                                    <p>The majority of posts within my chosen folksonomies are users’ own posts. this means that these are the original thoughts all tweets of the users interacting with them these  folksonomies (#lockdownSouthAfrica, #loneliness, #belonging). This could prove that the tweets being sent out are more personal and involve users sharing their own personal details and experiences. The second-highest type of post is replies. this indicates that conversations are forming around these folksonomies. use this could be replying to one another's posts in order to find out more information, offer support, or simply express that they can relate. Lastly, users are sharing one another's posts. This could be in order to draw attention to specific instances that are taking place, or as a way to express that one relates to what the original user was saying in their post.</p>
                                </div>
                            </div>
                        </section>
                    </section>
                </div>

            </div>
        </Layout>
    )
}
