import Link from 'next/link'

import Layout from '../components/Layout'
import Banner from '../components/Banner'
import Contact from '../components/Contact'
import Algorithm from '../components/Algorithm'
import Cities from '../components/Cities'
import {Pie, Bar} from 'react-chartjs-2';
import Cloud from '../components/Cloud'
import Next from '../components/Next'

export default () => {

    var options = {
        legend: {
            position: "bottom",
            labels: {
                usePointStyle: true,
                fontSize: 20,
                padding	: 20,
            }
        },
        segmentShowStroke : false
    };

    const data4 = {
        labels: [
            'Negative',
            'Neutral',
            'Positive'
        ],
        datasets: [{
            data: [152, 403, 625],
            backgroundColor: [
                "#A6A6A8",
                "#B1E5F2",
                "#272635",
                "#CECECE",
                "#E8E9F3",
                "#B0DE09"
            ],
            hoverBackgroundColor: [
                "#A6A6A8",
                "#B1E5F2",
                "#272635",
                "#CECECE",
                "#E8E9F3",
                "#B0DE09"
            ],
            borderWidth: 0
        }],
    }

    const barData = {
        labels: ['#lockdownSouthAfrica', '#loneliness', '#belonging'],
        datasets: [
          {
            label: 'Male',
            backgroundColor: '#A6A6A8',
            borderColor: '#A6A6A8',
            borderWidth: 1,
            hoverBackgroundColor: '#A6A6A8',
            hoverBorderColor: '#A6A6A8',
            data: [44.32, 44.32, 38.22]
          }, {
          label: 'Female',
          backgroundColor: '#B1E5F2',
          borderColor: '#B1E5F2',
          borderWidth: 1,
          hoverBackgroundColor: '#B1E5F2',
          hoverBorderColor: '#B1E5F2',
          data: [55.68, 55.68, 61.78],
          borderWidth: 0
        },
        ]
    };

    const barData2 = {
        labels: ['Own', 'Replies', 'Shares'],
        datasets: [
          {
            label: 'Amount of Content',
            borderWidth: 1,
            data: [55.7, 24.84, 19.46],
            backgroundColor: [
                "#A6A6A8",
                "#B1E5F2",
                "#272635",
                "#CECECE",
                "#E8E9F3",
                "#B0DE09"
            ],
            hoverBackgroundColor: [
                "#A6A6A8",
                "#B1E5F2",
                "#272635",
                "#CECECE",
                "#E8E9F3",
                "#B0DE09"
            ],
            borderWidth: 0
          },
        ]
    };

    const pieData2 = {
        labels: [
            'Engagements',
            'Impressions'
        ],
        datasets: [{
            data: [311, 458],
            backgroundColor: [
                "#A6A6A8",
                "#B1E5F2",
                "#272635",
                "#CECECE",
                "#E8E9F3",
                "#B0DE09"
            ],
            hoverBackgroundColor: [
                "#A6A6A8",
                "#B1E5F2",
                "#272635",
                "#CECECE",
                "#E8E9F3",
                "#B0DE09"
            ],
            borderWidth: 0
        }],
    }

    return(
    <Layout>
        <div>
            <Banner />

            <Contact />

            <div className="inner">
                <h1>Who is Belonging Bot?</h1>
                <p>Belonging Bot is a Twitter Bot that interacts with other Twitter users that are discussing topics such as: Loneliness, Belonging and the Lockdown in South Africa. 
                It does this to try to understand social connectedness online further, by analysing Twitter interactions with the help of Folksonomies and Sentiment Analysis!<br/><br/>
                Once it understands the experiences of different communities in South Africa during this time, my bot will try to understand how and why conversations between users areperpetuated. 
                Belonging Bot attempts to encourage the sharing of positive posts by responding to and retweeting them - adding imagery and emojis to include a visual break from all of the text that Twitter is usually made up of.</p>
            </div>

            <div id="main">
                <section id="one" className="tiles">
                    <article className="pie">
                        <Pie
                            data={data4}
                            width={200}
                            height={200}
                            options={options}
                        />
                        <header className="major">
                            <h3>Sentiment Analysis</h3>
                            <p>of selected Folksonomies</p>
                        </header>
                        <Link href="/sentiment"><a className="link primary"></a></Link>
                    </article>
                    <article className="bar">
                        <Bar
                            data={barData}
                            width={200}
                            height={200}
                            options={options}
                        />
                        <header className="major">
                            <h3>Demographics</h3>
                            <p>of different Folksonomies</p>
                        </header>
                        <Link href="/demographic"><a className="link primary"></a></Link>
                    </article>
                    <article className="bar">
                        <Bar
                            data={barData2}
                            width={200}
                            height={200}
                            options={options}
                        />
                        <header className="major">
                            <h3>Type</h3>
                            <p>Content type</p>
                        </header>
                        <Link href="/types"><a className="link primary"></a></Link>
                    </article>
                    <article className="pie">
                        <Pie
                            data={pieData2}
                            width={200}
                            height={200}
                            options={options}
                        />
                        <header className="major">
                            <h3>Interactions</h3>
                            <p>Impressiong and Engagemnent</p>
                        </header>
                        <Link href="/interactions"><a className="link primary"></a></Link>
                    </article>
                </section>
            </div>

            <div className="inner">
                <h1>Belonging bot follower algorithm</h1>
                <Algorithm />
            </div>

            <div className="inner">
                <h2>Main South African cities where different folksonomies are used</h2>
                <Cities />
            </div>

            <div className="inner">
                <h2>Hashtags associated wiith different Folksonomies</h2>
                <Cloud />
            </div>

            <div className="inner">
                <h2>What's next?</h2>
                <Next />
            </div>

        </div>
    </Layout>
)
    }
