import Layout from '../components/Layout'
import Banner from '../components/Banner'
import Definitions from '../components/Definitions'

export default () => (
    <Layout>
        <div>
            <Banner />

            <Definitions />

        </div>
    </Layout>
)
