import Head from "next/head"
import Select from 'react-select'

import Layout from '../components/Layout'
import BannerDemo from '../components/BannerDemo'
import { useState } from "react"

const options = [
    { value: 'lockdownSouthAfrica', label: '#lockdownSouthAfrica' },
    { value: 'loneliness', label: '#loneliness' },
    { value: 'belonging', label: '#belonging' },
];

export default () => {
    const [tag, setTag] = useState('lockdownSouthAfrica')

    const handleChange = selectedOption => {
        setTag(selectedOption.value)
    };

    return (
        <Layout>
            <Head>
                <title>Folksonomy Demographics</title>
                <meta name="description" content="Folksonomy Demographics" />
            </Head>

            <div>
                <BannerDemo />

                <div id="main">
                    <section id="one">
                        <div className="inner">
                            <header className="major">
                                <h2>Demographics within different Folksonomies</h2>
                            </header>
                            <p>An exploration of the genders, ages, cities and interests of the people who interacted within the folksonomies that I chose, and how they see themselves.</p>
                            <Select
                                value={tag}
                                onChange={selectedOption => handleChange(selectedOption)}
                                options={options}
                                placeholder={tag}
                                theme={(theme) => ({
                                    ...theme,
                                    borderRadius: 0,
                                    colors: {
                                    ...theme.colors,
                                    text: 'black',
                                    neutral0: '#272635',
                                    primary25: '#B1E5F2',
                                    primary: 'black',
                                    },
                                })}
                            /> <br/><br/>

                            <h2>#{tag}</h2>
                        </div>
                    </section>
                    <section id="two" className="spotlights">
                        <section>
                            <div className="chart" style={{padding: '7% 40px'}}>
                                {tag === 'lockdownSouthAfrica' && (
                                    <>
                                    <img src='/static/images/girl.png' style={{width: '3vw'}} />  &nbsp;&nbsp; 44.32% <br/><br/>
                                    <img src='/static/images/man.png' style={{width: '3vw'}} />  &nbsp;&nbsp; 55.68%
                                    </>
                                )}
                                {tag === 'loneliness' && (
                                    <>
                                    <img src='/static/images/girl.png' style={{width: '3vw'}} />  &nbsp;&nbsp; 44.32% <br/><br/>
                                    <img src='/static/images/man.png' style={{width: '3vw'}} />  &nbsp;&nbsp; 55.68%
                                    </>
                                )}
                                {tag === 'belonging' && (
                                    <>
                                    <img src='/static/images/girl.png' style={{width: '3vw'}} />  &nbsp;&nbsp; 61.78% <br/><br/>
                                    <img src='/static/images/man.png' style={{width: '3vw'}} />  &nbsp;&nbsp; 38.22%
                                    </>
                                )}
                            </div>
                            <div className="content">
                                <div className="inner">
                                    <header className="major">
                                        <h3>Gender</h3>
                                    </header>
                                    <br/><br/>
                                    {tag === 'lockdownSouthAfrica' && (
                                        <p>Of the users who identified their gender within this folksonomy, more than half of them were male. This could be due to the fact that the majority of Twitter users in South Africa are male. When looking at the types of tweets shared within this  folksonomy,  as well as the accounts from which they are shared,  many of them come from news representatives and different companies. According to the Department of Statistics South Africa, female representation in the workplace is still below 50% in South Africa. This could explain why these tweets are predominantly shared by men. </p>
                                    )}
                                    {tag === 'loneliness' && (
                                        <p>More than half of the users who interacted in the folksonomy #loneliness were men.  According to Psychology Today, women base their friendships around emotional attachment where men based their friendships around activities.  because of this, men may gravitate towards social media platforms, such as Twitter, to gain the emotional support that they are lacking from the people that they interact with in their everyday lives. In searching for this emotional connection, they may express that they are experiencing loneliness,  which is why they may interact within this folksonomy.</p>
                                    )}
                                    {tag === 'belonging' && (
                                        <p>Of the users who interacted within the folksonomy #belonging, the majority were female. This could be due to the fact that, as explained in Psychology Today, women create and maintain friendships through making emotional connections. This means that they are more likely to share their feelings with one another. because of the time frame in which my data was collected, many people could not spend time with their friends and families due to the societal lockdown. This could have led two women on Twitter reaching out in order to find emotional connection and belonging online. </p>
                                    )}
                                </div>
                            </div>
                        </section>
                        <section>
                            <div className="chart" style={{padding: '7% 40px'}}>
                                {tag === 'lockdownSouthAfrica' && (
                                    <>
                                        <img src='/static/images/belong-age.png' style={{width: '8vw'}} />  &nbsp;&nbsp; 28.99%
                                    </>
                                )}
                                {tag === 'loneliness' && (
                                    <>
                                        <img src='/static/images/lonely-age.png' style={{width: '8vw'}} />  &nbsp;&nbsp; 57.38%
                                    </>
                                )}
                                {tag === 'belonging' && (
                                    <>
                                        <img src='/static/images/belong-age.png' style={{width: '8vw'}} />  &nbsp;&nbsp; 40.36%
                                    </>
                                )}
                            </div>
                            <div className="content">
                                <div className="inner">
                                    <header className="major">
                                        <h3>Age</h3>
                                    </header>
                                    {tag === 'lockdownSouthAfrica' && (
                                        <p>The reason that the age group of 18 to 24 was the largest represented within  this folksonomy  could be due to the fact that their schooling, work and social lives were very much disrupted. With many 18 year olds around the country entering into matric in 2020,  the lockdown would have greatly disadvantaged their ability to learn in a traditional classroom setting,  and could have put them at a disadvantage. This age group would also represent many people who are new to the workplace,  and the inability to go to work could be detrimental to their newfound careers.  Lastly,  this demographic would usually socialise outside of a school or family environment. This means that the societal lockdown would also affect their social lives, and they may want to tweet about their experiences or frustrations. </p>
                                    )}
                                    {tag === 'loneliness' && (
                                       <p>More than half of the users who interacted within the folksonomy #loneliness were between the ages of 25 to 36 years old. This could be due to the fact that for many of them this is the first time in their careers that they have had to work from home. the lack of interaction between co-workers could have been causing them to experience feelings of loneliness during the week. These users have often also moved out of their parents homes and may live alone, causing even more loneliness with regards to the societal lockdown. The user to interact within this folksonomy may be looking for camaraderie in the form of other Twitter users who are also experiencing this sensation, or maybe looking for a community with which to interact and alleviate the loneliness. </p>
                                    )}
                                    {tag === 'belonging' && (
                                         <p>Of the users who interacted with in the folksonomy belonging, the highest age group was 18 to 24 years old. this could be for a number of reasons. This age group might be either experiencing university or the work environment for the first time and they feel as though they do not fit in and search for belonging online. This group may be struggling with having to partake in online learning for the first time due to the societal lockdown and thus may be reaching out. People in this age group are also likely to interact with friends outside of their own or their parents homes. Because of the societal lockdown, and the decreased ability to do this, they may be looking for social interaction on Twitter.</p>
                                    )}
                                </div>
                            </div>
                        </section>
                        <section>
                            <div className="chart" style={{padding: '7% 40px'}}>
                                {tag === 'lockdownSouthAfrica' && (
                                    <>
                                        <img src='/static/images/lock-city.png' style={{width: '8vw'}} />  &nbsp;&nbsp; 28.99%
                                    </>
                                )}
                                {tag === 'loneliness' && (
                                    <>
                                        <img src='/static/images/lonely-city.png' style={{width: '8vw'}} />  &nbsp;&nbsp; 57.38%
                                    </>
                                )}
                                {tag === 'belonging' && (
                                    <>
                                        <img src='/static/images/lock-city.png' style={{width: '8vw'}} />  &nbsp;&nbsp; 40.36%
                                    </>
                                )}
                            </div>
                            <div className="content">
                                <div className="inner">
                                    <header className="major">
                                        <h3>Popular City</h3>
                                    </header>
                                    {tag === 'lockdownSouthAfrica' && (
                                        <p>The  folksonomy lockdown South Africa what's especially popular in the city of Cape Town. This could be due to the fact that the societal lockdown that was entered into because of covid-19 was especially detrimental to their  tourist market. Many people visit Cape Town from all over the world to see sites such as Table Mountain, Robben Island and it's many beautiful beaches. With the lack of income generated from The Tourist industry,  many individuals and businesses suffered.  because of this people may have taken to Twitter in order to voice their concern or frustrations.  news media outlets in Cape Town would also have wanted to keep residents and businesses up-to-date. </p>
                                    )}
                                    {tag === 'loneliness' && (
                                        <p>The city in which most users who interacted with in the folksonomy #loneliness reside is Johannesburg. The residents of this specific city could be experiencing heightened feelings of loneliness during the societal lockdown due to the fact that many activities in Johannesburg consist of leaving one's home and going to different restaurants, parks, markets or museums. Johannesburg is the city that consists of mostly young residents with over 50% being under 30 years old. Because a lot of the activities in which they would partake would be things such as going out and having a good time with their friends, the inability to do this could cause feelings of loneliness, thus leading them to vent their frustrations in an online space. </p>
                                    )}
                                    {tag === 'belonging' && (
                                        <p>The most popular city in which the folksonomy #belonging was interacted with was Cape Town. This could be due to the fact that Cape Town has many outdoor activities in which to partake, which would have been shut down or limited due to the  societal lockdown. Another reason could be because Cape Town has multiple universities in and surrounding it. the students at these universities could have been sweating within this folksonomy in order to find people with which to interact online during the lockdown, or to find other students who are facing similar challenges as themselves.</p>
                                    )}
                                </div>
                            </div>
                        </section>
                        <section>
                            <div className="chart" style={{padding: '7% 40px'}}>
                                {tag === 'lockdownSouthAfrica' && (
                                    <>
                                        <img src='/static/images/lock-interest.png' style={{width: '8vw'}} />  &nbsp;&nbsp; 28.99%
                                    </>
                                )}
                                {tag === 'loneliness' && (
                                    <>
                                        <img src='/static/images/lock-interest.png' style={{width: '8vw'}} />  &nbsp;&nbsp; 57.38%
                                    </>
                                )}
                                {tag === 'belonging' && (
                                    <>
                                        <img src='/static/images/beelong-interest.png' style={{width: '8vw'}} />  &nbsp;&nbsp; 40.36%
                                    </>
                                )}
                            </div>
                            <div className="content">
                                <div className="inner">
                                    <header className="major">
                                        <h3>Interests</h3>
                                    </header>
                                    {tag === 'lockdownSouthAfrica' && (
                                        <p>Travel was the most commonly listed interest by users who interacted within the  folksonomy #lockdownSouthAfrica. This could be due to the fact but the lockdown stipulated a ban on travel. Users may have had plans to visit different countries or cities, or have friends and family from different places visit them. Because this was unable to happen, users may have gone to Twitter to voice their frustrations under the hashtag #lockdownSouthAfrica.</p>
                                    )}
                                    {tag === 'loneliness' && (
                                        <p>The main interest of the users who interacted with in the folksonomy #loneliness  what's travel. More than 50% of users stated their interest in this topic. The reason that so many people who are interested in traveling, may be tweeting about loneliness could be due to the time frame in which I collected the tweets. I started to collect the tweets during the time that the societal lockdown was in place. Because of this people could not travel overseas or locally. Users who tweeted about loneliness and had an interest in trouble may have been upset but they had to cancel certain plans because of the lockdown. Travelers may have wanted to see friends or family in different countries or provinces and may not have been able to.</p>
                                    )}
                                    {tag === 'belonging' && (
                                        <p>A large number of users who interacted with in the folksonomy #belonging had an interest in Society, with 14.36% of users listing it. This could allude to the fact that many users who are tweeting about belonging are aiming to discover how it is affecting others. The users interacting within folksonomies could be anything from scholars to empaths. Having an interest in society suggests having an interest in the way that people function within it. Because the sense of belonging is found through fitting in with one's peers, users who are interested in society aim to find out why and how this happens. </p>
                                    )}
                                </div>
                            </div>
                        </section>
                        <section>
                            <div className="chart" style={{padding: '7% 40px'}}>
                                {tag === 'lockdownSouthAfrica' && (
                                    <>
                                    <h3>Covid</h3>
                                    <h3>Lockdown</h3>
                                    <h3>South</h3>
                                    </>
                                )}
                                {tag === 'loneliness' && (
                                    <>
                                    <h3>Lonely</h3>
                                    <h3>Love</h3>
                                    <h3>World</h3>
                                    </>
                                )}
                                {tag === 'belonging' && (
                                    <>
                                    <h3>South</h3>
                                    <h3>Africa</h3>
                                    <h3>South Africa</h3>
                                    </>
                                )}
                            </div>
                            <div className="content">
                                <div className="inner">
                                    <header className="major">
                                        <h3>Describing themselves</h3>
                                    </header>
                                    {tag === 'lockdownSouthAfrica' && (
                                        <p>Twitter users who interacted with in the folksonomy lockdown South Africa had three main words with which they describe themselves in their Twitter bios. The first is Covid. The reason that this word was so commonly used within the bios  was because of the fact but the lockdown only happened because of the widespread pandemic:  covid-19. Most users who interacted within this folksonomy were speaking about the pandemic and the effects of it. The second word is lockdown.  this is because #lockdownSouthAfrica refers to the societal lockdown that was implemented to stop the spread of covid-19.  South was the third word that was most commonly used. The reason for this is because this folksonomy speaks about and is popular with users from South Africa. </p>
                                    )}
                                    {tag === 'loneliness' && (
                                        <p>Twitter users to interact with in the  folksonomy #loneliness described themselves most often with the following three words:  lonely, love and world. The reason that these users would describe themselves with the word lonely is due to the fact that loneliness describes the state of feeling lonely. The uses made describe themselves using the word love due to the fact that they believe but they are lacking love and this could be the reason that they are experiencing loneliness. Lastly, these users may use the word world when describing themselves because they are in search of other ways to alleviate the loneliness. </p>
                                    )}
                                    {tag === 'belonging' && (
                                        <p>The top three words that uses used to describe themselves within the folksonomy #belonging may not seem to be related to the folksonomy itself, as they are South, Africa and South Africa. Whilst this may not seem related, these tweets were only collected within the region of South Africa.  the fact that these users included the country within their bio and not only included the location, could speak to the facts that they are looking for a sense of belonging within their community. The act of including one's country name in their Twitter bio could act as a way to reach out to others within the country and attempt to find a way to relate to them.</p>
                                    )}
                                </div>
                            </div>
                        </section>
                    </section>
                </div>

            </div>
        </Layout>
    )
}
