// import GirlImg from '../static/images/girl.png'
import { useState } from 'react';
import Select from 'react-select'
import {Pie} from 'react-chartjs-2';

const options = [
  { value: 'lockdownSouthAfrica', label: '#lockdownSouthAfrica' },
  { value: 'loneliness', label: '#loneliness' },
  { value: 'belonging', label: '#belonging' },
];

const Cities = (props) => {
  const [tag, setTag] = useState('lockdownSouthAfrica')

  const handleChange = selectedOption => {
    setTag(selectedOption.value)
  };

  var options2 = {
    legend: {
      position: "bottom",
      labels: {
        usePointStyle: true,
        fontSize: 10,
        padding	: 15,
    }
    },
    segmentShowStroke : false
  };

  const dataLockdown = {
    labels: ['Cape Town', 'Johannesburg', 'Durban', 'Pretoria', 'Port Elizabeth', 'Lydenburg', 'Sandton', 'Polokwane', 'Parys'],
    datasets: [{
        data: [33.85, 24.62, 9.23, 4.62, 3.08, 1.54, 1.54, 1.54, 1.54],
        backgroundColor: [
            "#A6A6A8",
            "#B1E5F2",
            "#272635",
            "#CECECE",
            "#E8E9F3",
            "#B0DE09",
            "#8c99aa",
            '#707d8d',
            '#e6e8eb',
            '#d7e5ed'
        ],
        hoverBackgroundColor: [
            "#A6A6A8",
            "#B1E5F2",
            "#272635",
            "#CECECE",
            "#E8E9F3",
            "#B0DE09",
            "#8c99aa",
            '#707d8d',
            '#e6e8eb',
            '#d7e5ed'
        ]
    }]
  };
  
  const dataLonely = {
    labels: ['Johannesburg', 'Cape Town', 'Pretoria', 'Durban', 'Port Elizabeth', 'Rustenburg', 'Polokwane', 'Parys', 'Boksburg', 'Atlantis'],
    datasets: [{
        data: [15.89, 15.23, 13.25, 7.95, 3.97, 2.65, 2.65, 2.65, 1.99, 1.99],
        backgroundColor: [
            "#A6A6A8",
            "#B1E5F2",
            "#272635",
            "#CECECE",
            "#E8E9F3",
            "#B0DE09",
            "#8c99aa",
            '#707d8d',
            '#e6e8eb',
            '#d7e5ed'
        ],
        hoverBackgroundColor: [
            "#A6A6A8",
            "#B1E5F2",
            "#272635",
            "#CECECE",
            "#E8E9F3",
            "#B0DE09",
            "#8c99aa",
            '#707d8d',
            '#e6e8eb',
            '#d7e5ed'
        ]
    }]
  };

  const dataBelong = {
    labels: ['Cape Town', 'Johannesburg', 'Pretoria', 'Durban', 'Port Elizabeth', 'Stellenbosch', 'Bloemfontein', 'East London', 'Centurion', 'Polokwane'],
    datasets: [{
        data: [32.66, 24.26, 11.30, 7.24, 2.76, 1.51, 1.39, 1.02, 0.97, 0.88],
        backgroundColor: [
            "#A6A6A8",
            "#B1E5F2",
            "#272635",
            "#CECECE",
            "#E8E9F3",
            "#B0DE09",
            "#8c99aa",
            '#707d8d',
            '#e6e8eb',
            '#d7e5ed'
        ],
        hoverBackgroundColor: [
            "#A6A6A8",
            "#B1E5F2",
            "#272635",
            "#CECECE",
            "#E8E9F3",
            "#B0DE09",
            "#8c99aa",
            '#707d8d',
            '#e6e8eb',
            '#d7e5ed'
        ]
    }]
  };

  return (
    <>
    <Select
      value={tag}
      onChange={selectedOption => handleChange(selectedOption)}
      options={options}
      placeholder={tag}
      theme={(theme) => ({
        ...theme,
        borderRadius: 0,
        colors: {
        ...theme.colors,
          text: 'black',
          neutral0: '#272635',
          primary25: '#B1E5F2',
          primary: 'black',
        },
      })}
    />
    <br/><br/>

      <p style={{textAlign: 'center'}}>#{tag}</p>
      {tag === 'lockdownSouthAfrica' && (
        <div className="pie-explaination">
          <div className="cityPie" >
            <Pie
              data={dataLockdown}
              width={200}
              height={200}
              options={options2}
            />
          </div>
          <p>#lockdownSouthAfrica was more widely shared in larger metropolitan areas.This could be for one of 2 reasons.
            The first is that these areas are more likely to have a large variety of news and media networks operating in them, meaning
            that information on COVID-19 and the societal restrictions that it has caused would be shared by a variety of different
            sources. These sources would use this folksonomy to ensure that the information is easily available and found.
            The second reason is the fact that these areas are more densly populated, so their citizens would be more used to interacting
            with one another and thus feel the lack of real-world social connectedness more than people who do not often interact with others.
          </p>
        </div>
      )}
      {tag === 'loneliness' && (
        <div className="pie-explaination">
          <div className="cityPie" >
            <Pie
              data={dataLonely}
              width={200}
              height={200}
              options={options2}
            />
          </div>
          <p>#loneliness was more widely shared in larger metropolitan areas. This could be because of the fact that people in cities often 
            have more access to the internet, so they would be more likely to interact on social media. The second reason is the fact that 
            these areas are more densly populated, so their citizens would be more used to interacting with one another and thus feel the lack 
            of real-world social connectedness more than people who do not often interact with others.
          </p>
        </div>
      )}
      {tag === 'belonging' && (
        <div className="pie-explaination">
          <div className="cityPie" >
            <Pie
              data={dataBelong}
              width={200}
              height={200}
              options={options2}
            />
          </div>
          <p>#belonging was a widely spread out Folksonomy. Similarly to the others, it was more widely shared in larger metropolitan areas. Once again,
            this could be due to the fact that a more densly populated area may have more Twitter users or due to the fact that a denser population 
            would result in more users expreiencing the effects of the societal lockdown on their real-life relationships. An interesting insight to 
            #belonging is that it is popular in cities in which Universities operate. This could allude to the fact that students that may be expreiencing
            changes in their lives - as University is a large change when compared to the years of High School that come before - could be feeling a 
            lack of social connectedness. These students could be looking for a place online to meet like-mineded people and create a sense of social connectedness.
          </p>
        </div>
      )}
    {/* </div> */}

    <br/><br/>
    </>
  )
}

export default Cities
