// import GirlImg from '../static/images/girl.png'
import { useState } from 'react';
import Select from 'react-select'
import ReactWordcloud from 'react-wordcloud'
import TagCloud from 'react-tag-cloud'

import wordsLonely from './lonelinesswords'
import wordsLockdown from './lockdownwords'
import wordsBelong from './belongingwords'

const options = [
  { value: 'lockdownSouthAfrica', label: '#lockdownSouthAfrica' },
  { value: 'loneliness', label: '#loneliness' },
  { value: 'belonging', label: '#belonging' },
];

const Cloud = (props) => {
  const [tag, setTag] = useState('lockdownSouthAfrica')

  const handleChange = selectedOption => {
    setTag(selectedOption.value)
  };

  return (
    <section>
      <Select
        value={tag}
        onChange={selectedOption => handleChange(selectedOption)}
        options={options}
        theme={(theme) => ({
          ...theme,
          borderRadius: 0,
          colors: {
          ...theme.colors,
            text: 'black',
            neutral0: '#272635',
            primary25: '#B1E5F2',
            primary: 'black',
          },
        })}
      />
      <br/><br/>

        <p>#{tag}</p>
        {tag === 'lockdownSouthAfrica' && (
          <>
            <div className="pie-explaination">
              <TagCloud
                style={{
                  fontFamily: 'sans-serif',
                  fontSize: 30,
                  fontWeight: 'bold',
                  padding: 5,
                  width: '87vw',
                  height: '400px'
                }}>
                <div style={{fontSize: 64}}>#NewProfilePic</div>
                <div style={{fontSize: 80, color: '#B1E5F2'}}>#COVID19</div>
                <div style={{fontSize: 46}}>#zamisto</div>
                <div style={{fontSize: 36}}>#veganmuffin</div>
                <div style={{fontSize: 28}}>#vegan</div>
                <div style={{fontSize: 29}}>#summermenu</div>
                <div style={{fontSize: 72}}>#staysafe</div>
                <div style={{fontSize: 68}}>#stayhome</div>
                <div style={{fontSize: 48}}>#smooth</div>
                <div style={{fontSize: 55}}>#saota</div>
              </TagCloud>
            </div>
            <br/><br/>
            <p>When looking at the hashtags that are associated with the folksonomy #lockdownSouthAfrica, we can see that #COVID19 is the most used, more popular one. This is because the lockdown was only implemented due to the pandemic. Another two hashtags that were very popular within this folksonomy are #staysafe and #stayhome. These were initiatives that were launched in order to get people to abide by the regulations set out in the lockdown. They show that users care about their fellow citizens, and are aiming to stop the spread of the disease. </p>
          </>
        )}
        {tag === 'loneliness' && (
          <>
            <div className="pie-explaination">
              <TagCloud 
                style={{
                  fontFamily: 'sans-serif',
                  fontSize: 30,
                  fontWeight: 'bold',
                  padding: 5,
                  width: '87vw',
                  height: '400px'
                }}>
                <div style={{fontSize: 64}}>#Senekal</div>
                <div style={{fontSize: 58}}>#youpromisedtomarryme</div>
                <div style={{fontSize: 76}}>#worldmentalhealthday</div>
                <div style={{fontSize: 79, color: '#B1E5F2'}}>#speakoutagainstracism</div>
                <div style={{fontSize: 59}}>#orlandopirates</div>
                <div style={{fontSize: 78}}>#hatespeech</div>
                <div style={{fontSize: 52}}>#countrydut</div>
                <div style={{fontSize: 67}}>#ZonkeMchunu</div>
              </TagCloud>
            </div>
            <br/><br/>
            <p>The hashtags most commonly associated with the folksonomy #loneliness centre around putting an end to racism and hate speech. This can be seen with hashtags such as #speakoutagainstracism and #hatespeech. Being discriminated against because of the colour of your skin is a harrowing experience that can lead to feelings of loneliness. Oher hashtags associated with this folksonomy centre around  television entertainment. This could be because people are spending more time doing these activities alone because of the pandemic. </p>
          </>
        )}
        {tag === 'belonging' && (
          <>
            <div className="pie-explaination">
              <TagCloud 
                style={{
                  fontFamily: 'sans-serif',
                  fontSize: 30,
                  fontWeight: 'bold',
                  padding: 5,
                  width: '87vw',
                  height: '400px'
                }}>
                <div style={{fontSize: 64, color: '#B1E5F2'}}>#told</div>
                <div style={{fontSize: 11}}>#mistake</div>
                <div style={{fontSize: 16}}>#thought</div>
                <div style={{fontSize: 17}}>#bad</div>
              </TagCloud>
            </div>
            <br/><br/>
            <p>The hashtags that were most popular within the folksonomy #belonging did not create a cohesive story in the way that the other folksonomies seemed to do. This could be because belonging is so different to each individual person, and the thoughts expressed on Twitter around the subject do not follow any specific pattern. Hashtags such as #told and #thought show that users may be sharing their own experiences and stories around the topic of belonging. </p>
          </>
        )}
      <br/><br/>

    </section>
  )
}

export default Cloud
