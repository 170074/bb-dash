const BannerDemo = (props) => (
    <section id="banner" className="style2">
        <div className="inner">
            <header className="major">
                <h1>Folksonomy Demographics</h1>
            </header>
            <div className="content">
                <p>A look into the types of people interacting<br />
                within different folksonomies.</p>
            </div>
        </div>
    </section>
)

export default BannerDemo
