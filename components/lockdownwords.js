export default [
  {
    text: '#NewProfilePic',
    value: 64,
  },
  {
    text: '#COVID19',
    value: 80,
  },
  {
    text: '#zamisto',
    value: 46,
  },
  {
    text: '#veganmuffin',
    value: 36,
  },
  {
    text: '#vegan',
    value: 28,
  },
  {
    text: '#summermenu',
    value: 29,
  },
  {
    text: '#staysafe',
    value: 72,
  },
  {
    text: '#stayhome',
    value: 68,
  },
  {
    text: '#smoooth',
    value: 48,
  },
  {
    text: '#saota',
    value: 55,
  }
]