const Next = (props) => (
    <section>
        <div className="content">
            <div className="inner">
                {/* <header className="major">
                    <h3>Overall sentiment of collected Tweets</h3>
                </header> */}
                <p>With regards to future research, this study can be built upon to better incorporate social media users from different locations and within different folksonomies into online communities, by fostering a senense of social connectedness. Through this research, I discovered which users are reaching out in search of social connectedness in the form of conversations online. I aim to encourage online spaces to become more accepting of these users, and to better integrate them into online communities. Discovering specific folksonomies, as well as different geographic locations, in which users feel isolated or lonely could be done through surveys and interacting with more users. These users could come from a wider range of folksonomies, geographical locations, and social media platforms. By discovering the shortcomings within these spaces, social connectedness could be better fostered online and, by interacting within different folksonomies, and on different platforms, I would be able to see if the trends that I identified correspond.</p>
            </div>
        </div>
    </section>
)

export default Next
