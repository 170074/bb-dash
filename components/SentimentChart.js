import { useState } from 'react'
const SentimentChart = (props) => {
  
  const [tag, setTag] = useState('lockdown')

  const change = (value) => {
    setTag(value)
    console.log('tag');
  }

  return(
    <section>
        <div className="inner">
          <select name="tags" id="tags" onChange={(value) => change(value)} value="lockdown">
            <option value="lockdown">#lockdownSouthAfrica</option>
            <option value="lonely">#loneliness</option>
            <option value="belong">#belonging</option>
          </select>
          <br/><br/>
          <img className="graph" src='../static/images/lockdownsouthafrica/bysentiment.png' alt="lockdownsentiment" />
          <img className="graph" src='../static/images/loneliness/bysentiment.png' alt="lockdownsentiment" />
          <img className="graph" src='../static/images/belong/bysentiment.png' alt="lockdownsentiment" />
        </div>
    </section>
  )
}

export default SentimentChart
