const BannerSentiment = (props) => (
    <section id="banner" className="style2">
        <div className="inner">
            <header className="major">
                <h1>Sentiment Analysis</h1>
            </header>
            <div className="content">
                <p>What sentiments are most common for the<br />
                chosen folksonomies?</p>
            </div>
        </div>
    </section>
)

export default BannerSentiment
