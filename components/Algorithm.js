const Algorithm = (props) => (
  <section id="algo">
      <div className="inner"><br/><br/> 
        <pre>
          <code>if &#40; <strong style={{fontSize: '45px'}}>Follower Count</strong> &#60; 30 &amp;&amp; <strong style={{fontSize: '45px'}}>Sentiment Score</strong> &#60; 0 &#41; &#123; <br/>
          &nbsp;&nbsp;&nbsp;&nbsp; Users are more likely to connect with Belonging Bot <br/>
        &#125;
        </code></pre><br/><br/>
        <p>Belonging Bot provided me with insights with regards to how and why users visit a profile. The act of exploring another user’s page, and finding similar interests and experiences could be a factor in gaining a feeling of social connectedness. Maslow’s Third Need states that people will aim to form part of a group. One of the first steps to being included in a group is to share a similar interest. By having users click on Belonging Bot’s page, it can be assumed that they found a post interesting and relevant to themselves and chose to explore more. </p> 
        <br/><br/> 
        <p>When analysing the tweets that  Belonging Bot interacted with, accounts with more friends than followers often posted more tweets with a positive sentiment than negative. This could be as a result of the user feeling a sense of social connectedness, as they have found a community with which to interact on Twitter. The creations and interaction within this community satisfy Maslow’s third need, as interpersonal connections have been established. Interestingly, the majority of Belonging Bot’s followers were users who had posted tweets with negative sentiments and had Belonging Bot add them to a collection. This could show that users posting tweets with negative sentiments could be attempting to foster connections online and be aiming to form part of a community.</p>
        <br/><br/> 
      </div>
  </section>
)

export default Algorithm