const Banner = (props) => (
    <section id="banner" className="major">
        <div className="inner">
            <header className="major">
                <h1>Belonging Bot</h1>
            </header>
            <div className="content">
                <p>A means to understand social connectedness online <br/> through folksonomies &amp; sentiment analysis on Twitter.</p>
                <ul className="actions">
                    <li>
                        <a className="button next scrolly"
                           href="https://twitter.com/BelongingBot"
                           target="_blank"
                           data-size="large">
                           Follow @BelongingBot
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
)

export default Banner
