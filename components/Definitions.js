const Definitions = (props) => (
  <section>
      <div className="inner">
          <section style={{maxHeight: '600px !important'}}>
            <h1>Key Terms</h1>
          </section>
          <section className="split">
            <p>
              Sentiment Analysis
              <br/><br/>
              Sentiment analysis is a way to treat “opinions, sentiments and subjectivity of text” through computational processes (Medha, Hassan &amp; Korashy 2014:1). Thus, it is a way to discover people's opinions towards subject-matter online.
            </p>
            <br/><br/>
            <p>
              Folksonomies
              <br/><br/>
              A folksonomy is a way in which items on the internet are described and categorised by users (Neal 2007:7). This is done by attaching keywords to posts through the use of tags and hashtags, to structure and organise them.
            </p>
            <br/><br/>
            <p>
              Social Connectedness
              <br/><br/>
              In the article, “How Social Are Social Media? A Review of Online Social Behaviour and Connectedness”(2017) by Tracii Ryan, Kelly A. Allen, DeLeon L. Gray and Dennis M. McInerney, social connectedness is defined as feeling as though one belongs in the same group as, and relates to, one’s peers (2017:1).
            </p>
          </section>
      </div>
  </section>
)

export default Definitions
