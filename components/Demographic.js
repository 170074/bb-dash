// import GirlImg from '../static/images/girl.png'

const Demographic = (props) => (

  <section id="demo" className="major">
    <div>
      <h2>#loneliness</h2>
      <img src='/static/images/girl.png' style={{width: '3vw'}} />  &nbsp;&nbsp; 44.32% <br/><br/>
      <img src='/static/images/man.png' style={{width: '3vw'}} />  &nbsp;&nbsp; 55.68%
    </div>
    <div>
      <h2>#lockdownSouthAfrica</h2>
      <img src='/static/images/girl.png' style={{width: '3vw'}} />  &nbsp;&nbsp; 44.32% <br/><br/>
      <img src='/static/images/man.png' style={{width: '3vw'}} />  &nbsp;&nbsp; 55.68%
    </div>
    <div>
      <h2>#belonging</h2>
      <img src='/static/images/girl.png' style={{width: '3vw'}} />  &nbsp;&nbsp; 61.78% <br/><br/>
      <img src='/static/images/man.png' style={{width: '3vw'}} />  &nbsp;&nbsp; 38.22%
    </div>
  </section>
)

export default Demographic
