const About = (props) => (
  <section id="contact">
      <div className="inner">
          <section style={{maxHeight: '600px !important'}}>
            <h1>What's Belonging Bot about?</h1>
          </section>
          <section className="split">
            <p>Belonging Bot is a Twitter Bot that interacts with other Twitter users that are discussing topics such as: Loneliness, Belonging and the Lockdown in South Africa.<br/><br/>
            It does this to try to understand social connectedness online further, by analysing Twitter interactions with the help of Folksonomies and Sentiment Analysis!<br/><br/>
            Once it understands the experiences of different communities in South Africa during this time, my bot will try to understand how and why conversations between users areperpetuated.<br/><br/>
            Belonging Bot attempts to encourage the sharing of positive posts by responding to and retweeting them - adding imagery and emojis to include a visual break from all of the text that Twitter is usually made up of.<br/><br/></p>
            <br/><br/>
          </section>
      </div>
  </section>
)

export default About
