const BannerInteractions = (props) => (
    <section id="banner" className="style2">
        <div className="inner">
            <header className="major">
                <h1>Interactions</h1>
            </header>
            <div className="content">
                <p>What interactions have users had<br />
                with Belonging Bot?</p>
            </div>
        </div>
    </section>
)

export default BannerInteractions
