const Contact = (props) => (
    <section id="contact">
        <div className="inner">
            <section style={{maxHeight: '600px !important'}}>
                <a className="twitter-timeline" href="https://twitter.com/BelongingBot?ref_src=twsrc%5Etfw" style={{maxHeight: '400px !important', overflow: 'scroll'}}>Tweets by BelongingBot</a> <script async src="https://platform.twitter.com/widgets.js" charSet="utf-8"></script>
            </section>
            <section className="split">
                <section>
                    <div className="contact-method">
                        <span className="icon alt fa-user-plus"></span>
                        <h3>21 Followers</h3>
                    </div>
                </section>
                <section>
                    <div className="contact-method">
                        <span className="icon alt fa-user"></span>
                        <h3>101 Following</h3>
                    </div>
                </section>
                <section>
                    <div className="contact-method">
                        <span className="icon alt fa-twitter"></span>
                        <h3>192 Tweets</h3>
                    </div>
                </section>
                <section>
                    <div className="contact-method">
                        <span className="icon alt fa-thumbs-up"></span>
                        <h3>265 Likes</h3>
                    </div>
                </section>
            </section>
        </div>
    </section>
)

export default Contact
