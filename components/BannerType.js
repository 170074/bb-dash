const BannerTypes = (props) => (
    <section id="banner" className="style2">
        <div className="inner">
            <header className="major">
                <h1>Types of Interactions</h1>
            </header>
            <div className="content">
                <p>How are users interacting within<br />
                different folksonomies?</p>
            </div>
        </div>
    </section>
)

export default BannerTypes
