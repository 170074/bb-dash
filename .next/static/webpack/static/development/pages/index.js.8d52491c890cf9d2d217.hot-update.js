webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/Cities.js":
/*!******************************!*\
  !*** ./components/Cities.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-select */ "./node_modules/react-select/dist/react-select.browser.esm.js");
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-chartjs-2 */ "./node_modules/react-chartjs-2/es/index.js");
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__);
var _this = undefined,
    _jsxFileName = "/Users/ashleighparsons/Downloads/bb-dash/components/Cities.js";



function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? Object(arguments[i]) : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

// import GirlImg from '../static/images/girl.png'



var options = [{
  value: 'lockdownSouthAfrica',
  label: '#lockdownSouthAfrica'
}, {
  value: 'loneliness',
  label: '#loneliness'
}, {
  value: 'belonging',
  label: '#belonging'
}];

var Cities = function Cities(props) {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])('lockdownSouthAfrica'),
      _useState2 = _slicedToArray(_useState, 2),
      tag = _useState2[0],
      setTag = _useState2[1];

  var handleChange = function handleChange(selectedOption) {
    setTag(selectedOption.value);
  };

  var options = {
    legend: {
      position: "bottom",
      labels: {
        usePointStyle: true,
        fontSize: 10,
        padding: 15
      }
    },
    segmentShowStroke: false
  };
  var dataLockdown = {
    labels: ['Cape Town', 'Johannesburg', 'Durban', 'Pretoria', 'Port Elizabeth', 'Lydenburg', 'Sandton', 'Polokwane', 'Parys'],
    datasets: [{
      data: [33.85, 24.62, 9.23, 4.62, 3.08, 1.54, 1.54, 1.54, 1.54],
      backgroundColor: ["#A6A6A8", "#B1E5F2", "#272635", "#CECECE", "#E8E9F3", "#B0DE09", "#8c99aa", '#707d8d', '#e6e8eb', '#d7e5ed'],
      hoverBackgroundColor: ["#A6A6A8", "#B1E5F2", "#272635", "#CECECE", "#E8E9F3", "#B0DE09", "#8c99aa", '#707d8d', '#e6e8eb', '#d7e5ed']
    }]
  };
  var dataLonely = {
    labels: ['Johannesburg', 'Cape Town', 'Pretoria', 'Durban', 'Port Elizabeth', 'Rustenburg', 'Polokwane', 'Parys', 'Boksburg', 'Atlantis'],
    datasets: [{
      data: [15.89, 15.23, 13.25, 7.95, 3.97, 2.65, 2.65, 2.65, 1.99, 1.99],
      backgroundColor: ["#A6A6A8", "#B1E5F2", "#272635", "#CECECE", "#E8E9F3", "#B0DE09", "#8c99aa", '#707d8d', '#e6e8eb', '#d7e5ed'],
      hoverBackgroundColor: ["#A6A6A8", "#B1E5F2", "#272635", "#CECECE", "#E8E9F3", "#B0DE09", "#8c99aa", '#707d8d', '#e6e8eb', '#d7e5ed']
    }]
  };
  var dataBelong = {
    labels: ['Cape Town', 'Johannesburg', 'Pretoria', 'Durban', 'Port Elizabeth', 'Stellenbosch', 'Bloemfontein', 'East London', 'Centurion', 'Polokwane'],
    datasets: [{
      data: [32.66, 24.26, 11.30, 7.24, 2.76, 1.51, 1.39, 1.02, 0.97, 0.88],
      backgroundColor: ["#A6A6A8", "#B1E5F2", "#272635", "#CECECE", "#E8E9F3", "#B0DE09", "#8c99aa", '#707d8d', '#e6e8eb', '#d7e5ed'],
      hoverBackgroundColor: ["#A6A6A8", "#B1E5F2", "#272635", "#CECECE", "#E8E9F3", "#B0DE09", "#8c99aa", '#707d8d', '#e6e8eb', '#d7e5ed']
    }]
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_1__["default"], {
    value: tag,
    onChange: function onChange(selectedOption) {
      return handleChange(selectedOption);
    },
    placeholder: tag,
    theme: function theme(_theme) {
      return _objectSpread({}, _theme, {
        borderRadius: 0,
        colors: _objectSpread({}, _theme.colors, {
          text: 'black',
          neutral0: '#272635',
          primary25: '#B1E5F2',
          primary: 'black'
        })
      });
    },
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 126,
      columnNumber: 5
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 142,
      columnNumber: 5
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 142,
      columnNumber: 10
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    style: {
      textAlign: 'center'
    },
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 144,
      columnNumber: 7
    }
  }, "#", tag), tag === 'lockdownSouthAfrica' && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "pie-explaination",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 146,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "cityPie",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 147,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__["Pie"], {
    data: dataLockdown,
    width: 200,
    height: 200,
    options: options,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 148,
      columnNumber: 13
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 155,
      columnNumber: 11
    }
  }, "#lockdownSouthAfrica was more widely shared in larger metropolitan areas.This could be for one of 2 reasons. The first is that these areas are more likely to have a large variety of news and media networks operating in them, meaning that information on COVID-19 and the societal restrictions that it has caused would be shared by a variety of different sources. These sources would use this folksonomy to ensure that the information is easily available and found. The second reason is the fact that these areas are more densly populated, so their citizens would be more used to interacting with one another and thus feel the lack of real-world social connectedness more than people who do not often interact with others.")), tag === 'loneliness' && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "pie-explaination",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 165,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "cityPie",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 166,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__["Pie"], {
    data: dataLonely,
    width: 200,
    height: 200,
    options: options,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 167,
      columnNumber: 13
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 174,
      columnNumber: 11
    }
  }, "#loneliness was more widely shared in larger metropolitan areas. This could be because of the fact that people in cities often have more access to the internet, so they would be more likely to interact on social media. The second reason is the fact that these areas are more densly populated, so their citizens would be more used to interacting with one another and thus feel the lack of real-world social connectedness more than people who do not often interact with others.")), tag === 'belonging' && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "pie-explaination",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 182,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "cityPie",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 183,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__["Pie"], {
    data: dataBelong,
    width: 200,
    height: 200,
    options: options,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 184,
      columnNumber: 13
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 191,
      columnNumber: 11
    }
  }, "#belonging was a widely spread out Folksonomy. Similarly to the others, it was more widely shared in larger metropolitan areas. Once again, this could be due to the fact that a more densly populated area may have more Twitter users or due to the fact that a denser population would result in more users expreiencing the effects of the societal lockdown on their real-life relationships. An interesting insight to #belonging is that it is popular in cities in which Universities operate. This could allude to the fact that students that may be expreiencing changes in their lives - as University is a large change when compared to the years of High School that come before - could be feeling a lack of social connectedness. These students could be looking for a place online to meet like-mineded people and create a sense of social connectedness.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 202,
      columnNumber: 5
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 202,
      columnNumber: 10
    }
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Cities);

/***/ })

})
//# sourceMappingURL=index.js.8d52491c890cf9d2d217.hot-update.js.map